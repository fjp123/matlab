function Q = intquad (n)
A1 = ones (n) ;
Q = [ A1 * -1 , A1 * exp(1); A1*pi, A1 ]  ;
end