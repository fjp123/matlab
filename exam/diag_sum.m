function x = diag_sum(A)
[m,n] = size(A) ;
a = min(m,n) ;
b = (a+1)/2 ;
A = A([1:a],[1:a]) ;
B = flip(A) ;
if mod(a,2) == 0
     x = sum(diag(A))+sum(diag(B)) ;
else
     x = sum(diag(A))+sum(diag(B))-A(b,b) ;
end
end